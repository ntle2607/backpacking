import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './AppRouter.jsx';
import { Router, useRouterHistory } from 'react-router';
import { createHistory } from 'history';

const history = useRouterHistory(createHistory)({ basename: '/' });
var appRouter = new AppRouter();
var routers = appRouter.getRouters();

ReactDOM.render((
    <Router history={history}>
        {routers}
    </Router>
), document.getElementById('root'));
