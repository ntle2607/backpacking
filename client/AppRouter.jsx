import React from 'react';
import { Route, IndexRoute } from 'react-router';

import MainLayout from './page/MainLayout.jsx';
import HomePage from './page/HomePage.jsx';

export default class AppRouter {

    getRouters() {
        return (
            <Route path="/" component={MainLayout}>
                <IndexRoute component={HomePage} />
            </Route>
        );
    }
}
