import React from 'react';
import Carousel from './Carousel.jsx';
import Navbar from './Navbar.jsx';
import List from './List.jsx';
import CONFIG from './Config';
export default class MainLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeMenu: Navbar.MENU_KEYS.ALL,
            searchKey: "",
            currentPage: 1,
            pagesNumber: 10,
            listData: []
        }
    }
    componentDidMount() {
        this.onclickMenu(Navbar.MENU_KEYS.ALL);
    }
    onclickMenu(menuName) {
        // code get data
        if (menuName == "post") {
            this.setState({
                currentPage: 1, pagesNumber: 5, activeMenu: menuName,
                listData: [{
                    image: "./image/vietnam-ha-giang-ma-pi-leng-pass.jpg",
                    title: "Ha Giangxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                    id: "1"
                },
                {
                    image: "./image/nha trang.jpg",
                    title: "Biển Nha Trang",
                    id: "2"
                }]
            });
        }
        if (menuName == "ask") {
            this.setState({ currentPage: 1, pagesNumber: 4, activeMenu: menuName, listData: [] });
        }
        if (menuName == "all") {
            this.setState({
                currentPage: 1, pagesNumber: 10, activeMenu: menuName,
                listData: [{
                    image: "./image/vietnam-ha-giang-ma-pi-leng-pass.jpg",
                    title: "Ha Giang", id: "1"
                },
                {
                    image: "./image/nha trang.jpg",
                    title: "Biển Nha Trang",
                    id: "2"
                }]
            });
        }
    }
    onSelectPage(pageNo) {
        // code get data
        this.setState({ currentPage: pageNo });
    }
    onclickBtSearchbyKey(menuKey, searchKey) {
        // code get data
        this.setState({ searchKey: searchKey, currentPage: 1, pagesNumber: 9, activeMenu: menuKey });
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <p />
                        <Carousel />
                        <p />
                        <Navbar activeMenu={this.state.activeMenu} onclickMenu={this.onclickMenu.bind(this)} onSelectPage={this.onSelectPage.bind(this)}
                            searchKey={this.state.searchKey} pagesNumber={this.state.pagesNumber} currentPage={this.state.currentPage}
                            onclickBtSearchbyKey={this.onclickBtSearchbyKey.bind(this)} />
                        <List data={this.state.listData} />
                        <div >
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
