import React from 'react';

export default class Slide extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: ["./image/vietnam-ha-giang-ma-pi-leng-pass.jpg", "./image/nha trang.jpg"],
            current: 0
        }
        this.onclickBackward = this.onclickBackward.bind(this);
        this.onclickForward = this.onclickForward.bind(this);
    }
    onclickBackward() {
        this.setState({ current: this.state.current - 1 });
    }
    onclickForward() {
        this.setState({ current: this.state.current + 1 });
    }
    render() {
        let backwardClass = this.state.current > 0 ? "enable" : "disable";
        let forwardClass = this.state.current < this.state.data.length - 1 ? "enable" : "disable";
        let backwardColor = this.state.current > 0 ? "navigator-color" : "disable-navigator-color";
        let forwardColor = this.state.current < this.state.data.length - 1 ? "navigator-color" : "disable-navigator-color";
        return (
            <div className={this.props.className + " slide-container"}>
                <div className={"slide-backward " + backwardClass} onClick={this.onclickBackward} >
                    <div className={"arrow-left " + backwardColor} ></div>
                    <div className={"arrow-tail " + backwardColor} ></div>
                    <div className={"arrow-tail " + backwardColor} ></div>
                    <div className={"arrow-tail " + backwardColor} ></div>
                </div>
                <div className={"slide-forward " + forwardClass} onClick={this.onclickForward}>
                    <div className={"arrow-tail " + forwardColor} ></div>
                    <div className={"arrow-tail " + forwardColor} ></div>
                    <div className={"arrow-tail " + forwardColor} ></div>
                    <div className={"arrow-right " + forwardColor} ></div>
                </div>
                <div className="page-indicator"></div>
                {
                    this.state.data.map((element, index) => {
                        return <img key={index} src={element} alt="" id={index} className={"slide-content " + (this.state.current == index ? "display-block" : "display-none")} />
                    })
                }
            </div >
        )
    }
}
Slide.defaultProps = {
    className: "",
}
