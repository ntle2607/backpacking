import React from "react";

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.maxPages = 6;
        this.state = {
            firstPage: 1,
            lastPage: this.maxPages < this.props.pagesNumber ? this.maxPages : this.props.pagesNumber,
            searchKey: this.props.activeMenu + Navbar.SEPARATOR + this.props.searchKey
        };
    }
    onclickMenu(menuName) {
        this.setState({
            searchKey: menuName + ": "
        });
        this.props.onclickMenu(menuName);
    }
    onclickNext() {
        if (this.props.currentPage < this.props.pagesNumber)
            this.onSelectPage(this.props.currentPage + 1);
    }
    onclickPrev() {
        if (this.props.currentPage > 1)
            this.onSelectPage(this.props.currentPage - 1);
    }
    onchangeSearchKey(event) {
        this.setState({
            searchKey: event.target.value
        });
    }
    onclickBtSearchbyKey() {
        let searchKey = this.state.searchKey;
        let separatorIdx = searchKey.indexOf(Navbar.SEPARATOR);
        let menuKey = separatorIdx > 0 ? Navbar.MENU_KEYS.findMenu(searchKey.slice(0, separatorIdx)) : Navbar.MENU_KEYS.ALL;

        let __searchKey = separatorIdx >= 0 ? searchKey.slice(separatorIdx + 1) : searchKey;
        this.setState({ searchKey: menuKey + ":" + __searchKey });
        this.props.onclickBtSearchbyKey(menuKey, __searchKey);
    }
    onSelectPage(pageNo) {
        this.props.onSelectPage(pageNo);
    }
    render() {
        let paginationTag = "";
        console.log("pagesNumber", this.props.pagesNumber);
        if (this.props.pagesNumber > 0) {
            let paginationLi = [];
            let firstPage = 1;
            let lastPage = this.props.pagesNumber;
            if (this.props.pagesNumber > this.maxPages) {
                let center = Math.ceil(this.maxPages / 2);
                let lastInterval = this.maxPages - center;
                let firstInterval = center - 1;
                if (this.props.currentPage > center && this.props.currentPage + lastInterval <= this.props.pagesNumber) {
                    lastPage = this.props.currentPage + lastInterval;
                    firstPage = this.props.currentPage - firstInterval;
                } else if (this.props.currentPage <= center) {
                    firstPage = 1;
                    lastPage = this.maxPages;
                } else if (this.props.currentPage + lastInterval > this.props.pagesNumber) {
                    lastPage = this.props.pagesNumber;
                    firstPage = this.props.pagesNumber - this.maxPages + 1;
                }
                paginationLi.push(<li key="prev" className={this.props.currentPage === 1 ? "disabled" : ""}
                    onClick={this.onclickPrev.bind(this)}><a href="#pev">Prev</a></li>);

            }

            for (let i = firstPage; i <= lastPage; i++) {
                paginationLi.push(<li key={i} className={i == this.props.currentPage ? "active" : ""}
                    onClick={this.onSelectPage.bind(this, i)}><a href={"#" + i}> {i} </a></li>);
            }
            if (this.props.pagesNumber > this.maxPages) {
                paginationLi.push(<li key="next" className={this.props.currentPage === this.props.pagesNumber ? "disabled" : ""}
                    onClick={this.onclickNext.bind(this)}><a href="#next">Next</a></li>);
            }
            paginationTag = <ul className=" pagination my-pagination">
                {paginationLi}
            </ul>;
        }
        return (
            <div className="row">
                <div className="col-md-12">
                    <nav className="navbar navbar-default" role="navigation">
                        <div className="navbar-header">

                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#nav-bar-menu">
                                <span className="sr-only">Toggle navigation</span><span className="icon-bar"></span><span className="icon-bar">
                                </span><span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#"> <span className="badge "> Inbox42</span></a>
                        </div>
                        <div className="collapse navbar-collapse" id="nav-bar-menu">
                            <ul id="menu" className="nav navbar-nav  nav-bars">

                                <li className={this.props.activeMenu == "all" ? "active" : " none "}>
                                    <a href="#1" onClick={this.onclickMenu.bind(this, "all")}>All posts</a>
                                </li>
                                <li className={this.props.activeMenu == "post" ? "active" : "  none"}>
                                    <a href="#1" onClick={this.onclickMenu.bind(this, "post")}>Post news</a>
                                </li>
                                <li className={this.props.activeMenu == "ask" ? "active" : " none"}>
                                    <a href="#2" onClick={this.onclickMenu.bind(this, "ask")}>Ask/Question</a>
                                </li>
                            </ul>
                            <form className="navbar-form navbar-left" role="search">
                                <div className="form-group">
                                    <input type="text" className="form-control" value={this.state.searchKey}
                                        onChange={this.onchangeSearchKey.bind(this)} />
                                </div>
                                <button type="submit" className="btn btn-default" onClick={this.onclickBtSearchbyKey.bind(this)}>
                                    Search
                                </button>
                            </form>
                            <ul className="nav navbar-nav navbar-right">
                                {/*<li className=" navbar-form ">
                                    {paginationTag}
                                </li>*/}
                                <li>
                                    <a href="#">Link</a>
                                </li>
                                <li className="dropdown">
                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">Order by<strong className="caret"></strong></a>
                                    <ul className="dropdown-menu">
                                        <li>
                                            <a href="#">Post date</a>
                                        </li>
                                        <li>
                                            <a href="#">Like</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </nav>
                </div>
            </div>
        );
    }
}

Navbar.MENU_KEYS = {
    ALL: "all",
    POST: "post",
    ASK: "ask",
    findMenu: (menuKey) => {
        let lowerMenuKey = menuKey.toLowerCase().trim();
        if (lowerMenuKey === this.ALL || lowerMenuKey === this.POST || lowerMenuKey === this.ASK)
            return lowerMenuKey;
        else
            return false;
    }
}
Navbar.SEPARATOR = ":";
Navbar.defaultProps = {}

export default Navbar
