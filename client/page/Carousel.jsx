import React from "react";

export default class Carousel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [{ src: "./image/vietnam-ha-giang-ma-pi-leng-pass.jpg", desc: <span>Hà Giang <br />Địa đầu Tổ Quốc</span>, href: "http://north-vietnam.com/ha-giang/" },
            { src: "./image/nha trang.jpg", desc: "Biển Nha Trang", href: "https://www.vietnamonline.com/destination/nha-trang.html" }],
            current: 0
        }
    }
    render() {
        let lis = [];
        let items = [];
        for (var i = 0; i < this.state.data.length; i++) {
            var element = this.state.data[i];
            lis.push(<li key={i} data-target="#my-carousel-container" data-slide-to={i} className={i == this.state.current ? "active" : ""} ></li>);
            items.push
                (<div key={i} className={i == this.state.current ? "item active" : "item"}>
                    <a href={element.href}>
                        <img src={element.src} alt="" className="my-carousel-img" ></img>
                        <div className="carousel-caption"><h4>{element.desc}</h4></div>
                    </a>
                </div>);
        }
        return (
            <div className="row ">
                <div className="col-md-12">
                    <div id="my-carousel-container" data-ride="carousel" data-interval="false"
                        className={this.props.className + " carousel slide"} >
                        <ol className="carousel-indicators" > {lis} </ol>
                        <div className="carousel-inner" role="listbox"> {items} </div>
                        <a className="left carousel-control" href="#my-carousel-container" role="button" data-slide="prev">
                            <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="right carousel-control" href="#my-carousel-container" role="button" data-slide="next">
                            <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
Carousel.defaultProps = {
    className: ""
}