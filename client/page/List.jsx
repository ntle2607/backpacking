import React from "react";
export default class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        console.log(this.props.data);
        return (
            <div className="row">
                <div className="col-md-12">
                    {this.props.data === null || this.props.data.length === 0 ? "No result" : ""}
                    {this.props.data.map((row) =>
                        <div key={row.id} className="my-list-row">
                            <div className="my-list-column-img">
                                <img className="my-img" alt={row.title} src={row.image} />
                            </div>
                            <div className="my-list-column-title">
                                <a href={'/post/' + row.id}>{row.title}</a>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}
List.defaultProps = {
    data: []
}