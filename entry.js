var express = require('express');
var fs = require('fs');
var app = express();

// private functions
var port = 8088;
// start app
app.listen(port);
// public static files
app.use(express.static('client/public'));

app.use(express.static('public'));
// routing
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/" + "index.html");
});

// Console will print the message
console.log('Server running at 127.0.0.1:' + port);
